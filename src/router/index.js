import { createRouter, createWebHistory } from 'vue-router'
import UserInfo from '../components/layout/UserInfo.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: UserInfo
    }
  ]
})

export default router
