import { defineStore } from 'pinia'
import { ref, watch } from 'vue'

export const useCommentsStore = defineStore('commentsStore', () => {
    const comments = ref([
        {
            // id: 1,
            name: 'Samuel Jackson',
            comment: 'Hey Eva! You are cool. Nice pic!',
            date: '13 Apr 2022'
        },
        {
            // id: 2,
            name: 'Angela Deimon',
            comment: 'Thanks for your services! We really liked the ocean view room. We hope to cooperate in the near future. We are sure you will do everything to make our next holiday fantastic.',
            date: '10 Apr 2022'
        },
        {
            // id: 3,
            name: 'Ronald Harris',
            comment: 'Eva, hello! There is such a question: How can I contact you if I am abroad in roaming?',
            date: '8 Apr 2022'
        }
    ])

    const commentsLocaleStorage = localStorage.getItem('comments')

    if(commentsLocaleStorage) {
        comments.value = JSON.parse(commentsLocaleStorage)._value
    }

    const setComments = (newComment) => {
        comments.value.push(newComment)
    }

    watch(() => comments, (state) => {
        localStorage.setItem('comments', JSON.stringify(state))
    }, { deep: true })

    return { comments, setComments }
})
